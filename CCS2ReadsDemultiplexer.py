#!/usr/bin/env python
import sys
import linecache   #to read specific line of the file
import argparse
from openpyxl import load_workbook   # pip install openpyxl
import csv
import re #exact string matching
import tre #fuzzy regex matching
#from subprocess import call
import collections
from Bio.Seq import Seq
from Bio import SeqIO
#import copy
import time
import Levenshtein
import os
#from Bio import pairwise2


class Welcome():
    def displayWelcomeMsg(self):
        print "-"*25;
        print "PACBIO barcode extractor";
        print "-"*25;

class Utilities():
    def fuzzy_str_regex_match(self, in_pattern, in_str, error):
        tp = tre.compile(in_pattern)
        fz = tre.Fuzzyness(maxerr=error)
        m = tp.search(in_str, fz)
        if m:
            return m.groups()[0][0], m.groups()[0][1]
        else:
            return None,None

    def levenshteinDistance(self, s1, s2):
        if len(s1) > len(s2):
            s1, s2 = s2, s1

        distances = range(len(s1) + 1)
        for i2, c2 in enumerate(s2):
            distances_ = [i2 + 1]
            for i1, c1 in enumerate(s1):
                if c1 == c2:
                    distances_.append(distances[i1])
                else:
                    distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
            distances = distances_
        return distances[-1]

    def progress_bar(self,i, t, start_t):
        sys.stdout.write('\tcompleted \r{:2.1f}%'.format(round(float(i + 1) / t * 100)))
        sys.stdout.flush()
        if i == 1000:
            e_time = time.time() - start_t
            print "\n\nINFO: Estimated time to step completion " + '{:2.3f}'.format(float(e_time) * float(t) / 1000 / 3600) +\
                  " hrs or "+ '{:2.0f}'.format(float(e_time) * float(t) / 1000 / 60) +" min\n"

    def check_mid_tags(self, field, mid_dict, df):
        avail_mids = [df[k][field].keys()[0] for k in df.keys()]
        #print avail_mids
        diff = set(mid_dict.keys()) - set(avail_mids)
        before_len = len(mid_dict)
        for k in diff:
            del mid_dict[k]
        after_len = len(mid_dict)
        if after_len <  before_len:
            print "INFO: "+field+" :some MID tags were removed to avoid false mapping (since not found in meta data)"
            print "INFO: "+field+" :reduced from "+str(before_len)+" to "+str(after_len)+"\n"
        return mid_dict

class FastqReads():
    nt_lines = 0 #file lines
    seq_lines = []
    n_seq_line = 0
    fastq_file = ""

    def __init__(self, filename):
        self.count_number_lines(filename)
        self.get_seq_lines_n()
        self.fastq_file = filename

    def count_number_lines(self, filename):
        with open(filename) as f:
            self.nt_lines = sum(1 for _ in f)

    def get_seq_lines_n(self):
        self.seq_lines = range(2,self.nt_lines, 4)

    def get_next_fastq_read(self):
        read_name = linecache.getline(self.fastq_file, self.seq_lines[self.n_seq_line]-1)
        seq = linecache.getline(self.fastq_file, self.seq_lines[self.n_seq_line])
        self.n_seq_line = self.n_seq_line+1
        return seq.rstrip("\n"), read_name



class ReadMetaInput():

    def read_excel(self, f):
        wb = load_workbook(f, read_only=True);
        ws = wb.active;
        #print ws.max_row;
        #print ws.max_column;

        title_line = [ws.cell(row=1, column=c).value for c in range(1,20)];
        search_headers = ["384PlateID","Processid", "Sampleid","Coordinate", "Forward MID", "Reverse MID"]
        try:
            c_idx =  [ title_line.index(name) for name in  search_headers] #extract the columns
        except ValueError,e:
            print str(e)
            sys.exit("ERROR: Make sure the header containts these columns: "+ str(search_headers))

        idx_dict = dict(zip (search_headers, c_idx))
        #print c_idx
        #print [i.index() for i in enumerate(title_line)]
        #print title_line

        #raw_data = [d for d in ws.iter_rows("A{}:J{}".format(2,ws.max_row))] # skip the 1st line design as header
        raw_data = [d for d in ws["A{}:Z{}".format(2, ws.max_row)]]  # skip the 1st line design as header
        #print raw_data
        #sys.exit(0)
        meta_data={}
        for r in range(0, len(raw_data)):
            plateid = raw_data[r][idx_dict["384PlateID"]].value  #processID
            pid = raw_data[r][idx_dict["Processid"]].value  #processID
            sid = (raw_data[r][idx_dict["Sampleid"]].value); #sampleID

            #sid="BIORTP-051-A01_GuHCl_GF_WB.Pacbio"
            if sid:
                sid=re.sub('\.Pac[B|b]io','',sid)
            else:
                sid=""
            if (pid is not None and len(pid)>0) or (sid is not None and len(sid)>0):
                #print "SID >"+sid+"<"; sys.exit()
                meta_data[r]= {"pid": pid, \
                               "sid": sid, \
                               "plateid": plateid,\
                               "crd": raw_data[r][idx_dict["Coordinate"]].value, \
                               "mid_f": {raw_data[r][idx_dict["Forward MID"]].value: ""},
                               "mid_r": {raw_data[r][idx_dict["Reverse MID"]].value: ""},
                               "bcodes": [],
                               #"bcodes_names": []
                               }
        return meta_data

    def read_tab_txt(self, f):
        with open(f) as f:
            reader = csv.reader(f, delimiter="\t");
            m_tags = {}
            for key, value in reader:
                m_tags[key]= value.rstrip("\n")
        return m_tags

    def combine_meta_data(self,d,fwd,rev):
        for i in range(0,len(d)):
            try:
                key = d[i]["mid_f"].keys()[0]
                d[i]["mid_f"][key] = fwd[key]
                key = d[i]["mid_r"].keys()[0]
                d[i]["mid_r"][key] = rev[key]
            except LookupError:
                print "Opps! There seems to be problem with the mid-tag keys. Some keys are missing ..."
                print "Key", key, "is missing"
                print d[i]
                sys.exit ("Aborting")
        return d



class ProcessFastqRead:
    #mid_len = 0 if not specified (whole read length is used)
    def __init__(self, error, mid_len):
        self.tag_f = ""
        self.tag_r = ""
        self.idx= None
        self.error=error  # number of mismatches
        self.mid_reg_len = mid_len


    def findMIDtags(self, read, mids, read_end_type):

        if self.mid_reg_len > 0:  #cut the read so that matching is done at the start and end of the read
            if read_end_type == "start":
                read = read[0:self.mid_reg_len]
            if read_end_type == "end":
                read = read[-self.mid_reg_len:]
            #read = fw_part+end_part
        #print read, len(read[-self.mid_reg_len:]), self.mid_reg_len, len(mids)
        for key in mids:
            if self.error != 0:  # works slow when exact match is done
                tp = tre.compile(mids[key])
                fz = tre.Fuzzyness(maxerr=self.error)
                m = tp.search(read, fz)
            else: # classical exact match implementation (faster)
                re.compile(mids[key])
                m = re.search(mids[key], read)
            if m is not None:
                return key

    def map2well(self, df, read, mid_fwd, mid_rev):

        self.tag_f = self.findMIDtags(read, mid_fwd,"start"); self.tag_r = self.findMIDtags(read, mid_rev, "end")


        if args.MIDmatchLogic[0] == "OR":
            self.idx = [i for i in range(0, len(df)) if df[i]["mid_f"].keys()[0] == self.tag_f or df[i]["mid_r"].keys()[0] == self.tag_r]
        else:
            self.idx = [i for i in range(0, len(df)) if df[i]["mid_f"].keys()[0] == self.tag_f and df[i]["mid_r"].keys()[0] == self.tag_r]
        #print self.idx, self.tag_f, self.tag_r
        #print "initial:"+ read
        #sys.exit()
        if self.idx:
            #print self.idx, self.tag_f, self.tag_r, read
            return read, self.idx[0], 1
        else:
            #print "RevComplement the read"
            rev_read = str(Seq(read).reverse_complement()) #correct orientation of the read
            #print rev_read
            self.tag_f = self.findMIDtags(rev_read, mid_fwd, "start"); self.tag_r = self.findMIDtags(rev_read, mid_rev, "end")
            #print self.tag_f, self.tag_r
            idx = [i for i in range(0, len(df)) if df[i]["mid_f"].keys()[0] == self.tag_f and df[i]["mid_r"].keys()[0] == self.tag_r]
            if idx:
                return rev_read, idx[0], 1 #case 2: rev matched
            else:
                return rev_read, idx, 0 #case 3: not matched idx = []



class WriteResultsDataOut():

    def __init__(self, f_out):
        with open(f_out, "w")  as fp:
            fp.close()

    def write_run_parameters(self, f_out, f_fastq_in, f_meta_in, m_threshold,m_read_len):
        with open(f_out, "a") as fp:
            fp.write('## Output\t{0!s}\n## FASTQ file\t{1!s}\n## Meta file\t{2!s}\n## Match threshold tolerance\t{3!s}\n## Match length in bp (0=all read len)\t{4!s}\n'.format(f_out,f_fastq_in, f_meta_in, m_threshold,m_read_len))
        fp.close()

    def overlap_dist2ref(self,bc, meta_rec, ref_seq_dict):
        u = Utilities()
        #print meta_rec
        sid = meta_rec["sid"]
        pid = meta_rec["pid"]

        #print sid, pid

        #re = re.compile(sid)
        #m = re.search(mids[key], read)
        #if m is not None:
        #    return key

        #Match to reference seq via Sample ID search
        k_ref = [k for k in ref_seq_dict.keys() if re.search(sid,k)]

        if k_ref:
            k_ref = k_ref[0]
        else:
            #Alternatively, match to reference seq via Process ID search
            k_ref = [k for k in ref_seq_dict.keys() if re.search(pid, k)]
            if k_ref:
                k_ref = k_ref[0]
            else:
                return "NoRef","NoRef","NoRef","NoRef","NoRef","NoRef"
        ref_seq = ref_seq_dict[k_ref]


        tp = tre.compile(ref_seq)
        fz = tre.Fuzzyness(maxerr=2)
        m = tp.search(bc, fz)

        #if m:
        #    start = m.groups()[0][0]; end = m.groups()[0][1]
        #    consensus_bc = bc[start:end]
        #else:
        #    return ">50", "", "", "", "", k_ref

        #tp = tre.compile(consensus_bc)
        #m = tp.search(ref_seq, fz)


        if m:
            #print "Option 1"
            start = m.groups()[0][0]; end = m.groups()[0][1]
            #print start, end
            #consenus_ref_seq = ref_seq[start:end]
            consenus = ref_seq[start:end]
        else:
            #print "Option 2"
            tp_ref = tre.compile(bc[0:20])
            m_ref = tp_ref.search(ref_seq, fz)
            if m_ref == None:
                return ">50", "", "", "", "", k_ref
            ref_seq = ref_seq[m_ref.groups()[0][0]:]

            tp = tre.compile(ref_seq[0:20])
            m = tp.search(bc, fz)
            if m:
                start_idx = m.groups()[0][0];
                end_idx = start_idx + min(len(ref_seq), len(bc))
                consenus= bc[start_idx:start_idx + end_idx]
            else:
                return ">50", "", "", "", "", k_ref

        #print len(consenus)
        ref_seq = ref_seq[0:len(consenus)]
        #calculate overlap distance
        #print consenus
        #print ref_seq
        #print bc
        #sys.exit()

        max_olp_len =  len(consenus)
        dist_obj = Levenshtein.editops(ref_seq, consenus)
        if len(dist_obj) == 0: #perfect match
            return  0, 0, 0, max_olp_len, 0, k_ref

        #breakdown distance to indels and subst counts
        dist = len(dist_obj)
        counter = collections.Counter(zip(*dist_obj)[0])
        editop_dict = dict(counter.most_common())
        indels=0; subs=0
        for k in editop_dict:
            #print k
            if k == "delete" or k == "insert":
                indels = indels+editop_dict[k];
            if k == "replace":
                subs = subs+editop_dict[k]

        error_rate = (float(dist)/float(max_olp_len))*100
        #print k_ref
        #print dist, indels, subs, max_olp_len, error_rate,  k_ref
        #sys.exit("CP1")

        return dist, indels, subs, max_olp_len, error_rate,  k_ref

        #sys.exit("Stopped")
            #matches2refs[k] = {"ref_len": m.groups()[0][1]-m.groups()[0][0], "dist": u.levenshteinDistance(seq, ref_seq_dict[k])}

        #if matches2refs:
            #print len(matches2refs)
            #print matches2refs
            #print matches2refs.items()
            #print matches2refs.values()
            #print [item["dist"] for item in matches2refs.values()]
        #    min_dist =  min([item["dist"] for item in matches2refs.values()])
        #    k = [k for k, v in matches2refs.items() if v["dist"] == min_dist][0]
        #    #print k, matches2refs[k]["dist"] , matches2refs[k]["ref_len"]
        #    return matches2refs[k]["dist"], k,  matches2refs[k]["ref_len"]
        #else:
        #    return  0,"NoMatch", 0

    def return_unq_bc(self,bcs):
        counter = collections.Counter(bcs)
        mc = counter.most_common()
        barcodes = []; bc_freq = []
        if mc:
            n = len(mc)
            barcodes= [mc[i][0] for i in range(0,n)]
            bc_freq = [mc[i][1] for i in range(0,n)]
        return barcodes, bc_freq

    def write_results_out(self, df, f_out, ref_seq_obj):
        print "\nWritting final output ..."
        print "DF has # barcode seqs: "+ str(sum([len(df[k]["bcodes"]) for k in df.keys() if len(df[k]["bcodes"]) > 0]))
        start_time = time.time()
        ut = Utilities()
        with open(f_out,"a") as fp:
            fp.write('{0!s}\t{1!s}\t{2!s}\t{3!s}\t{4!s}\t{5!s}\t{6!s}\t{7!s}\t{8!s}\t{9!s}\t{10!s}\t{11!s}\t{12!s}\t{13!s}\t{14!s}\n'.format("ProcessID","SampleID","PlateID","Coordinate", "AmpliconName", "DescriptiveID",
                                                                  "Barcode","Nreads","OvlpDist2Ref", "Indels", "Subst", "Len(bp)","MaxOverlap(bp)", "%Error2Ref", "RefSeqID"))
            counter_keys=0; total_wrote_lines=0
            for key in df.keys():  # row number of the meta data {0..n}
                ut.progress_bar(counter_keys,len(df.keys()),start_time)
                record = df[key]   # e.g. recod = {'mid_r': {u'0001_AsR_lbc76_rc': 'ACGTGAGCTCACTCGC'}, 'pid': u'PACBC001-16', 'bcodes': [], {u'0001_AsF_lbc13': 'ACACTGACGTCGCGAC'} ... }
                #print record

                #print [len(r) for r in record["bcodes"]]
                #print [len(b) for b in df[4793]["bcodes"]];
                #print len(df[4793]["bcodes"])
                #sys.exit()
                bcs, fq = self.return_unq_bc(record["bcodes"]) #return barcodes and frequencies
                if bcs:
                    for n in range(0,len(bcs)): #loop through all barcodes assigned to a well
                            if len(bcs[n]) > 0: #in some cases some records of the original data frame do not have any bcs due to failed MID-tag mapping
                                #print "Error in key: "+str(key);
                                #print "BC "+str(bcs[n]) + " and freq " + str(fq[n]) + "\n"
                                #print [len(r) for r in record["bcodes"]]
                                #fp.write('{0!s}\n'.format("ERROR: ZERO LENGTH BARCODE IDENTIFIED!\n"))
                                #sys.exit("Error zero length barcode in df key "+str(key))
                                if ref_seq_obj.ref_seq_dict:
                                    d2r, n_indels, n_subs, cons_len, error, ref_id  = self.overlap_dist2ref(bcs[n],record, ref_seq_obj.ref_seq_dict)
                                else: #if no refrence is
                                    d2r = " "; n_indels=" "; n_subs=" "; error=""; cons_len = " "; ref_id = " "

                                #print ref_id
                                fp.write('{0!s}\t{1!s}\t{2!s}\t{3!s}\t{4!s}\t{5!s}\t{6!s}\t{7!s}\t{8!s}\t{9!s}\t{10!s}\t{11!s}\t{12!s}\t{13!s}\t{14!s}\n'.format(str(record["pid"]),
                                    str(record["sid"]),str(record["plateid"]),str(record["crd"]), "",
                                    str(str(record["pid"])+"["+str(record["mid_f"].keys()[0])+"+"+str(record["mid_r"].keys()[0])+"]_"+str(n+1)+"_"+str(fq[n])+"reads"), bcs[n], \
                                                                        fq[n], d2r, n_indels, n_subs, len(bcs[n]), cons_len, error, ref_id))
                                total_wrote_lines +=1
                counter_keys = counter_keys + 1
            fp.close()
            print ("\nWrote total of "+str(total_wrote_lines)+" UNIQUE/SID barcodes to output file (no duplicates) ...")

    def write_reads2multi_fasta(self, df, out_dir):
        print out_dir
        pt = re.compile('(.+\/)'); path = pt.match(out_dir).group(0)+"multiFASTA-"+re.compile('(.+)(\/)(\w+.*)(.*.fastq)').match(args.in_fastq[0]).group(3)+"/"  #extract destination folder path
        try:
            if os.path.isdir(path) == False:
                os.mkdir(path)
            else:
                print "Path already exists"
        except:
            sys.exit("ERROR: Could not create directory at "+path)


        for k in df.keys():
            if len(df[k]["bcodes"]) > 0:
                fp = open(path+df[k]["sid"]+".fasta", "w")
                for i in range(0,len(df[k]["bcodes"])):
                    fp.write("{!s}\n{!s}\n".format(">"+df[k]["sid"]+"_"+str(i),df[k]["bcodes"][i]))
                fp.close()

        #print df[k]

    def clean_barcodes(self, df, pf, pr, nlen_fw, nlen_rev):
        d_dict={"W":"[A|T]","S":"[C|G]","M":"[A|C]","K":"[G|T]","R":"[A|G]","Y":"[C|T]",
                "B": "[C|G|T]","D":"[A|G|T]", "H":"[A|C|T]","V":"[A|C|G]","N":"[A|C|G|T]","A":"A","T":"T","G":"G","C":"C" } # degenerative dictionary
        error=5 #allow for a maximum 1 error in edit dist during the target gene primer match
        u=Utilities() #access fuzzy match functions


        # forward
        m_fwd_pattern = "".join([d_dict[l] for l in pf])  # forward match

        # reverse
        pr = str(Seq(pr).reverse_complement())  # reverse complement match
        m_rev_pattern = "".join([d_dict[l] for l in pr])




        for i in range(0,len(df)):
            #print i
            #DEBUG
            #i= [i for i in range(0,len(df)) if df[i]["sid"] == "BIOUG31189-C08"][0] #DEGUG

            barcodes = []

            #if i == [j for j in range(0, len(df)) if df[j]["sid"] == "BIOUG31189-C08"][0]:
            #    print "\nclean before\n"
            #    print df[i]


            for bc in df[i]["bcodes"]:
                #print bc, len(bc) #DEBUG
                if bc and len(bc) < 1000:
                    #forward
                    pos_fwd_s, pos_fwd_e  = u.fuzzy_str_regex_match(m_fwd_pattern,bc, error)
                    #reverse match
                    pos_rev_s, pos_rev_e = u.fuzzy_str_regex_match(m_rev_pattern, bc, error)

                    #DEBUG
                    #if i == [j for j in range(0, len(df)) if df[j]["sid"] == "BIOUG31189-C08"][0]:
                    #    print pos_fwd_s, pos_fwd_e, pos_rev_s, pos_rev_e

                    #print pos_fwd_s, pos_fwd_e, pos_rev_s, pos_rev_e
                    # option 1: chop by exact of fuzzy match
                    if pos_fwd_s and pos_fwd_e and pos_rev_s and pos_rev_e and len(bc[pos_fwd_e:pos_rev_s]) > 0:
                        #type_of_cleaning = "classical"
                        #print "option 1"
                        #print len(bc[pos_fwd_e:pos_rev_s])
                        barcodes.append(bc[pos_fwd_e:pos_rev_s])
                    # option 2: if only there is a match in the 5' end but not 3' end
                    elif pos_fwd_s and pos_fwd_e and pos_rev_s == None and pos_rev_e == None:
                        #type_of_cleaning = "only fwd 5' region worked"
                        barcodes.append(bc[pos_fwd_e:])
                    elif pos_fwd_s == None and pos_fwd_e == None and pos_rev_s and pos_rev_e:
                        #type_of_cleaning = "only rev 3' region worked"
                        barcodes.append(bc[:pos_rev_s])
                    # option 3: chop by length adding all "technical sequences" (brute-force)
                    else:
                        #print "option 2", len(bc), nlen_fw , len(bc)-nlen_rev
                        #if nlen_fw > 0 and abs(nlen_rev - nlen_fw) > 50 :  #make sure the resulting read is >50 bp long
                        #    type_of_cleaning = "bold length clean"
                        #    barcodes.append(bc[nlen_fw:len(bc)-nlen_rev])
                        #else:
                        #type_of_cleaning = "do nothing"
                        barcodes.append(bc) #do nothing if else fails (do not clean)


                #print [len(bc) for bc in barcodes]
                #barcodes = "" #DEBUG
            df[i]["bcodes"] = barcodes #write new barcodes (assigment by reference to df)




            #DEBUG
            #if i == [j for j in range(0, len(df)) if df[j]["sid"] == "BIOUG31189-C08"][0]:
                #print "\nclean after\n"
                #print df[[i for i in range(0, len(df)) if df[i]["sid"] == "BIOUG31189-C08"][0]]
                #print type_of_cleaning
                #exit()
            #print df[[i for i in range(0,len(df)) if df[i]["sid"] == "BIOUG31189-C08"][0]]
            #print barcodes, len(barcodes[0])
            #exit()


class Dist2ReferenceSeqs():

    def __init__(self):
        self.ref_seq_dict={}

    def read_fasta(self, in_f):
        with open(in_f, "rU") as fp:
            for record in SeqIO.parse(fp, "fasta"):
                #print record.description, record.annotations
                #print dir(record)
                self.ref_seq_dict[record.description] = str((record.seq).rstrip("-"))



if __name__ == '__main__':

    w=Welcome()
    w.displayWelcomeMsg()


    parser = argparse.ArgumentParser(description='Reads FASTQ raw CCS reads from PacBio sequencers and identifies per sample barcodes');
    parser.add_argument('-i', '--in-fastq', nargs=1, dest="in_fastq", default=None, metavar='', type=str, help = 'Input path to FASTQ file');
    parser.add_argument('-m', '--meta-data', nargs=1, dest="meta_file", default=None, metavar='', type=str, help='Input path to META file');
    parser.add_argument('-mid-fwd', '--mid-fwd-seqs', nargs=1, dest="mid_seq_fwd_file", default=None, metavar='', type=str, help='Input path to forward MID-tag sequences');
    parser.add_argument('-mid-rev', '--mid-rev-seqs', nargs=1, dest="mid_seq_rev_file", default=None, metavar='', type=str, help='Input path to reverse MID-tag sequences');
    parser.add_argument('-af', '--adapter-fw', dest="a_fw", nargs=1, default=None, metavar='', help='Input forward adapter sequence')
    parser.add_argument('-ar', '--adapter-rev', dest="a_rev", nargs=1, default=None, metavar='', help='Input reverse adapter sequence')
    parser.add_argument('-pf', '--primer-fw', dest="prim_fw", nargs=1, default=None, metavar='', help='Input Primer forward sequence')
    parser.add_argument('-pr', '--primer-rev', dest="prim_rev", nargs=1, default=None, metavar='', help='Input Primer reverse sequence')
    parser.add_argument('-rs', '--ref-seq', dest="ref_seq", nargs=1, default="", metavar='', help='Input file with reference barcode sequences (optional)')
    parser.add_argument('-ps', '--pad-seq', dest="pad_seq", nargs=1, default=None, metavar='', help='Input PAD sequence')
    parser.add_argument('-t', '--match-error', dest="m_error", nargs=1, type=int, default=None, metavar='', help='Max mismatch error tolerance (e.g., 0 = exact match)')
    parser.add_argument('-cleanReads', dest="cleanReads", nargs=1, type=str, default="Yes", help='Clean the reads from technical seqs (adapters, pad, mids, primer)')
    parser.add_argument('-writeReads2Fastas', dest="reads2Fastas", nargs=1, type=str, default="No", help='Write reads to corresponding fasta files named SampleID.fasta')
    parser.add_argument('-mid-match-logic', dest="MIDmatchLogic", nargs=1, type=str, default=["AND"], help='MID match logic (OR or AND). Default AND (both reverse and forward MIDs required to assign to SampleID)')
    parser.add_argument('-midLen', '--mid-tag-regLen', dest="mid_reg_len", nargs=1, type=int, default=[0], metavar='', help='Region lenght where to search for mid-tags (e.g. 0 all seq len)')
    parser.add_argument('-o', '--output', dest="output", nargs=1, default=None, metavar='', help='Output file to store results (e.g. output.txt)')

    global args
    args = parser.parse_args()
    print args

    if (args.MIDmatchLogic[0].rstrip() != "OR") and (args.MIDmatchLogic[0].rstrip() != "AND"):
        sys.exit("\nERROR: -mid-match-logic can have values or AND or OR. Ineligible value supplied ("+args.MIDmatchLogic[0]+"). Aborting ... ")

    print "Allowed error in fuzzy match: "+str(args.m_error[0])
    print "Input file name: " + str(args.in_fastq[0])
    print "Output file name: " + str(args.output[0])

    # check if any arguments are missing
    #for k in vars(args).keys():
    #    if vars(args)[k] == None:
    #        parser.print_help()
    #        sys.exit("ERROR: Missing "+str(k)+" parameter. Please specify one and try again!")


    meta=ReadMetaInput()

    m_fwd = meta.read_tab_txt(args.mid_seq_fwd_file[0]) #dict
    m_rev = meta.read_tab_txt(args.mid_seq_rev_file[0]) #dict


    df = meta.read_excel(args.meta_file[0])
    #print [df[i] for i in range(0,1)];
    #sys.exit()
    df = meta.combine_meta_data(df, m_fwd, m_rev) #add mid tag sequences to the meta data read from Excel file
    #print df[0]
    #sys.exit()


    ut = Utilities()
    m_fwd = ut.check_mid_tags("mid_f", m_fwd,df)
    m_rev = ut.check_mid_tags("mid_r", m_rev, df)




    fr = FastqReads(args.in_fastq[0]) #fastq object

    refseqs = Dist2ReferenceSeqs() #dictionary of the references keys = title line, values = seqs
    if args.ref_seq:
        refseqs.read_fasta(args.ref_seq[0])
    else:
        print "No reference sequences specified"

    wd = WriteResultsDataOut(args.output[0])
    wd.write_reads2multi_fasta(df, args.output[0])
    #print refseqs.ref_seq_dict;
    #print refseqs.ref_seq_dict.keys()
    #sys.exit("CP1")

    #process every fastq read
    #for ln in range(0,len(fr.seq_lines)):
    start_time = time.time()
    print "Start run time:"+str(time.ctime(start_time))
    total_reads = len(fr.seq_lines)

    #Debug
    #total_reads = 1000 #!!!!!!

    print "\nProcessing "+str(total_reads)+" reads from PacBio fastq file ..."

    reads_succeeded = 0  # number of reads that were successfully mapped to wells
    failed2map_reads_counter = 0 #number of failed to map MID tags
    for ln in range(0,total_reads):
        pr = ProcessFastqRead(args.m_error[0], args.mid_reg_len[0])
        read, read_name = fr.get_next_fastq_read()
        read, idx, stat = pr.map2well(df, read, m_fwd,m_rev)

        #print idx
        if idx != []: #if match for MID tags is found
            df[idx]["bcodes"].append(read.rstrip())
            #df[idx]["bcodes_names"].append(read_name.rstrip())
            #print reads_succeeded
            reads_succeeded += 1
        else:
            failed2map_reads_counter +=1

        ut.progress_bar(ln, total_reads,start_time)
        #if ln == 100:
        #    break
    end_time = time.time()

    if reads_succeeded == 0:
        sys.exit("ERROR: None of the reads were mapped out of "+str(total_reads))
    #sys.exit()
    #print df[0]
    #approx distances before barcode in each read
    non_bc_fwd_seq_len = len(args.a_fw[0])+len(args.prim_fw[0])+len(m_fwd[m_fwd.keys()[0]])+len(args.pad_seq[0]) #total bp before barcode in fwd direction
    non_bc_rev_seq_len = len(args.a_rev[0]) + len(args.prim_rev[0]) + len(m_rev[m_rev.keys()[0]]) + len(args.pad_seq[0])  # total bp before barcode in fwd direction


    wd.write_run_parameters(args.output[0], args.in_fastq[0],args.meta_file[0], args.m_error[0], args.mid_reg_len[0])
    #idx = [idx for idx in range(0, len(df)) if df[idx]["sid"] == "BIOUG31189-C08"][0] #DEBUG
    #print df[idx]
    if args.cleanReads[0] == "Yes":
        print "Cleaning reads"
        wd.clean_barcodes(df, args.prim_fw[0], args.prim_rev[0], non_bc_fwd_seq_len, non_bc_rev_seq_len)

    wd.write_results_out(df, args.output[0], refseqs)


    if args.reads2Fastas[0] == "Yes":
        wd.write_reads2multi_fasta(df,args.output[0])
    print "\nFailed to map reads using MID-tag pairs "+str(failed2map_reads_counter) + " out of "+ str(total_reads)
    print "\nSuccessfully processed "+str(reads_succeeded) + " out of "+str(total_reads) + " reads ("+str(float(reads_succeeded)/float(total_reads)*100)+" %)"

    if float(failed2map_reads_counter)/float(total_reads) > 0.6:
        print("WARNING: Low sample mapping success rate. Check that correct MID-tag mapping meta data file is supplied!")
    #fr.determine_fastq_seq_lines(in_fasta)

    print "Output written to "+ args.output[0]
    print "\nDone"
    print "\n\nEnd run time: " + str(time.ctime(end_time))
    #call(["vi", "./RESULTS_OUT/tmp.txt"])
    #print "PACBIO barcode extractor" % __file__







