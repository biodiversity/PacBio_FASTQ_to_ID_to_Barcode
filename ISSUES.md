1. Missing barcodes in the output such as below
```
## Output	./RESULTS_OUT/PACBB/PACBB_9830_6_results_pacbb99.99.txt									
## FASTQ file	./FASTA/PACBB/PACBB_FromMenloPark/pacbb99.99.fastq									
## Meta file	./FASTA/PACBB/PacBio_Primer_Associations_20161101.xlsx									
## Match threshold tolerance	6									
PlateID	Coordinate	DescriptiveID	Barcode	Nreads	OvlpDist2Ref	Indels	Subst	MaxOverlap(bp)	%Error2Ref	RefSeqID
PACBB1148-16	C11	PACBB1148-16[0097_AsF_lbc58+0018_AsR_lbc215_rc]_1_3472reads		3472	>50					JIABD013-16|BIOUG30469-F05|Evaniidae|COI-5P
```
