#!/usr/bin/env bash
source activate python_dev
echo "Starting OPP run ..."
t=0
#acc=999 #-midLen 25
fastq="job-297-OPP-FT-02_CLepFolF-CLepFolR_9pM_SQL-030_B01_CCS2-999-ccs.fastq"
fastq_in_path="/Users/kirill/PycharmProjects/PacBioCCS2ReadsDemultiplexer/DATA/OntarioProvParks/fastq/"${fastq}
out_file_results_name="OPP-FT-02_CLepFolF-CLepFolR_9pM_SQL-030_B01_CCS2-999"
meta_data_file="OPP-FT02_LIMS-plate-records_EVZ20171002.xlsx"


~/PycharmProjects/PacBioCCS2ReadsDemultiplexer/CCS2ReadsDemultiplexer.py  --in-fastq ${fastq_in_path} \
-mid-fwd "/Users/kirill/PycharmProjects/PacBioCCS2ReadsDemultiplexer/DATA/Primers/9999_fwd.txt" \
-mid-rev "/Users/kirill/PycharmProjects/PacBioCCS2ReadsDemultiplexer/DATA/Primers/9999_rev.txt" \
-m "/Users/kirill/PycharmProjects/PacBioCCS2ReadsDemultiplexer/DATA/OntarioProvParks/${meta_data_file}" \
-af "GCAGTCGAACATGTAGCTGACTCAGGTCAC" -ar "TGGATCACTTGTGCAAGCATCACATCGTAG" -pf "RKTCAACMAATCATAAAGATATTGG" \
-pr "TAAACTTCWGGRTGWCCAAAAAATCA" -ps "" \
-cleanReads No \
-writeReads2Fastas No \
-t $t \
-midLen 50 \
-o "/Users/kirill/PycharmProjects/PacBioCCS2ReadsDemultiplexer/RESULTS_OUT/OntarioProvParks/${out_file_results_name}_t${t}.txt"


#OPP-01_CLepFolF-CLepFolR_12pM_CCS2-999 (run ID 172)
#OPP-02_CLepFolF-CLepFolR_12pM_CCS2-999 (run ID 174)
#OPP-03_CLepFolF-CLepFolR_12pM_CCS2-999 (run ID 176)
#OPP-04_CLepFolF-CLepFolR_12pM_CCS2-999 (run ID 182)
#OPP-05_CLepFolF-CLepFolR_12pM_CCS2-999 (run ID XXX)
#OPP-06_CLepFolF-CLepFolR_12pM_CCS2-999 (run ID 205)
# job-208-OPP-07_CLepFolF-CLepFolR_12pM_CCS2-999-ccs.fastq OPP-07_CLepFolF-CLepFolR_12pM_CCS2-999 OPP07_LIMS-plate-records_EVZ20170718.xlsx (runID 208)
# job-210-OPP-08_CLepFolF-CLepFolR_12pM_CCS2-999-ccs.fastq OPP-08_CLepFolF-CLepFolR_12pM_CCS2-999 OPP08_LIMS-plate-records_EVZ20170718.xlsx (runID 210)
# job-212-OPP-09_CLepFolF-C_LepFolR_12pM_SQL-18_CCS2-999-ccs.fastq OPP-09_CLepFolF-C_LepFolR_12pM_SQL-18_CCS2-999 OPP09_LIMS-plate-records_EVZ20170719.xlsx (runID 212)
# job-207-OPP-012_CLepFoilF-CLepFoilR_12pM_CCS2-999-ccs.fastq  OPP-012_CLepFoilF-CLepFoilR_12pM_CCS2-999 OPP12_LIMS-plate-records_EVZ20170719.xlsx

# OPP-05_CLepFolF-CLepFolR_12pM_CCS2-999
# Failed to map reads using MID-tag pairs 29881 out of 94704
# Successfully processed 64823 out of 94704 reads (68.44800642 %)

# OPP-06_CLepFolF-CLepFolR_12pM_CCS2-999 (run ID 205)
# Failed to map reads using MID-tag pairs 23431 out of 66147
# Successfully processed 42716 out of 66147 reads (64.5773806824 %)

# OPP-07_CLepFolF-CLepFolR_12pM_CCS2-999
# Failed to map reads using MID-tag pairs 32154 out of 87972
# Successfully processed 55818 out of 87972 reads (63.4497340063 %)

# OPP-08_CLepFolF-CLepFolR_12pM_CCS2-999
# Failed to map reads using MID-tag pairs 24066 out of 67973
# Successfully processed 43907 out of 67973 reads (64.5947655687 %)

# OPP-09_CLepFolF-C_LepFolR_12pM_SQL-18_CCS2-999
# Failed to map reads using MID-tag pairs 21579 out of 82635
# Successfully processed 61056 out of 82635 reads (73.8863677618 %)

# OPP-012_CLepFoilF-CLepFoilR_12pM_CCS2-999
# Failed to map reads using MID-tag pairs 27176 out of 100666
# Successfully processed 73490 out of 100666 reads (73.0037947271 %)

# OPP-010_CLepFoilF-CLepFolR_12pM_SQL-16_CCS2-999  OPP10_LIMS-plate-records_EVZ20170719.xlsx
# job-215-OPP-010_CLepFoilF-CLepFolR_12pM_SQL-16_CCS2-999-ccs.fastq
# Failed to map reads using MID-tag pairs 28922 out of 95565
# Successfully processed 66643 out of 95565 reads (69.7357819285 %)


# OPP-011_CLepFoilF-CLepFolR_12pM_SQL-16_CCS2-999
# job-216-OPP-11_CLepFolF-C_LepFolR_12pM_SQL-018_CCS2-999-ccs.fastq
# Failed to map reads using MID-tag pairs 17066 out of 57683
# Successfully processed 40617 out of 57683 reads (70.4141601512 %)

# OPP-013_CLepFolF-CLepFolR_10pM-SQL-019_CCS2-999
# job-224-OPP-013_CLepFolF-CLepFolR_10pM-SQL-019_CCS2-999-ccs.fastq
# OPP13_LIMS-plate-records_EVZ20170804.xlsx
#Failed to map reads using MID-tag pairs 24872 out of 86365
#Successfully processed 61493 out of 86365 reads (71.2012968216 %)

# OPP-014_CLepFolF-CLepFolR_10pM_SQL-019_CCS2-999
# job-225-OPP-014_CLepFolF-CLepFolR_10pM_SQL-019_CCS2-999-ccs.fastq
# OPP14_LIMS-plate-records_EVZ20170804.xlsx

# OPP-015_CLepFolF-CLepFolR_10pM_SQL-019_CCS2-999
# OPP15_LIMS-plate-records_EVZ20170804.xlsx
# job-228-OPP-015_CLepFolF-CLepFolR_10pM_SQL-019_CCS2-999-ccs.fastq

# OPP-016_CLepFolF-CLepFolR_10pM_SQL-019_CCS2-999
# OPP16_LIMS-plate-records_EVZ20170804.xlsx
# job-229-OPP-016_CLepFolF-CLepFolR_10pM_SQL-019_CCS2-999-ccs.fastq



#OPP17_LIMS-plate-records_EVZ20170804.xlsx
#job-231-OPP-017_CLepFolF-CLepFolR-9pM_SQL-023_CCS2-999-ccs.fastq

#OPP18_LIMS-plate-records_EVZ20170804.xlsx
#job-234-OPP-018_CLepFolF-CLepFolR-9pM_SQL-023_CCS2-999-ccs.fastq

#OPP19_LIMS-plate-records_EVZ20170804.xlsx
#job-236-OPP-019_CLepFolF-CLepFolR-9pM_SQL-023_CCS2-999-ccs.fastq

#OPP20_LIMS-plate-records_EVZ20170804.xlsx
#job-237-OPP-020_CLepFolF-CLepFolR-9pM_SQL-023_CCS2-999-ccs.fastq

#OPP29_LIMS-plate-records_EVZ20170914.xlsx
#job-284-OPP-029_CLepFolF-CLepFolR_9pM_SQL-029_A01_CCS2-999-ccs.fastq

#-------------------------------
#OPP-FT-01_CLepFolF-CLepFolR_9pM_SQL-030_A01_CCS2-999
#job-296-OPP-FT-01_CLepFolF-CLepFolR_9pM_SQL-030_A01_CCS2-999-ccs.tar.gz
#Successfully processed 91710 out of 141129 reads (64.9831005676 %)

#OPP-FT-02_CLepFolF-CLepFolR_9pM_SQL-030_B01_CCS2-999
#job-297-OPP-FT-02_CLepFolF-CLepFolR_9pM_SQL-030_B01_CCS2-999-ccs.tar.gz
#Failed to map reads using MID-tag pairs 68167 out of 206579
#Successfully processed 138412 out of 206579 reads (67.0019701906 %)

# Append taxonomy submitted to BOLD
# =MATCH(A2,Sheet1!$A$1:$A$9900,0)
# =IF(LEN(INDEX(Sheet1!E:E,K2,1))>0,INDEX(Sheet1!E:E,K2,0),IF(LEN(INDEX(Sheet1!D:D,K2,1))>0,INDEX(Sheet1!D:D,K2,1),INDEX(Sheet1!C:C,K2,0)))
