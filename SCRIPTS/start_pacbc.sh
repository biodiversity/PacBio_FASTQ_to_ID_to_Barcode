echo "Starting Thomas PACBC analysis"
t=4
./main.py -midLen 25 --in-fastq "./FASTA/PACBC/99.99.fastq" -m "./PACBIO/PACBC_9120records_MIDtoSampleAssociation_SwapCorrected_EVZ20161221.xlsx" -af "GCAGTCGAACATGTAGCTGACTCAGGTCAC" \
-ar "TGGATCACTTGTGCAAGCATCACATCGTAG" -pf "RKTCAACMAATCATAAAGATATTGG" -pr "TAAACTTCWGGRTGWCCAAAAAATCA" -ps "GGTAG" -rs "./FASTA/PACBC/9120_PacBio_ref_seqs.fasta" \
-t $t -o "./RESULTS_OUT/ThomasResults/PACBC_thomas_t${t}_9999.txt"
