#!/usr/bin/env bash
source activate python_dev
t=2
job=0
echo "Alk and SPRI analysis job ${job}"


root_dir="/Users/kirill/PycharmProjects/PacBio_QC_seqs"
${root_dir}/main.py -midLen 25 --in-fastq ${root_dir}/DATA/May17/test.fastq \
-mid-fwd "/Users/kirill/PycharmProjects/PacBio_QC_seqs/PACBIO/9999_fwd.txt" \
-mid-rev "/Users/kirill/PycharmProjects/PacBio_QC_seqs/PACBIO/9999_rev.txt" \
-m "${root_dir}/DATA/May17/CCDB-SQL-11-plate-layouts-2017-05-15-Primer_mappingV2.xlsx" -af "GCAGTCGAACATGTAGCTGACTCAGGTCAC" \
-ar "TGGATCACTTGTGCAAGCATCACATCGTAG" -pf "RKTCAACMAATCATAAAGATATTGG" -pr "TAAACTTCWGGRTGWCCAAAAAATCA" -ps "" \
-rs "${root_dir}/DATA/May17/References/Alk-SPRI_sanger_ref_seq.fasta" \
-t $t \
-o "${root_dir}/RESULTS_OUT/Alkaline_SPRI_lysis_May17/JobID_${job}_t${t}.txt"
