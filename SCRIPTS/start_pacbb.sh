#!/usr/bin/env bash
./main.py --in-fastq "./FASTA/PACBB/PACBB.fastq" -m "./FASTA/PACBB/PacBio_Primer_Associations_20161101.xlsx" \
-af "GCAGTCGAACATGTAGCTGACTCAGGTCAC" -ar "TGGATCACTTGTGCAAGCATCACATCGTAG" -pf "RKTCAACMAATCATAAAGATATTGG" \
-pr "TAAACTTCWGGRTGWCCAAAAAATCA" -ps "GGTAG" -rs "./FASTA/PACBB/9173_Sanger_ref_seqs.fasta" -t 2 -o "./RESULTS_OUT/PACBB_9830_resultsV1.txt"

