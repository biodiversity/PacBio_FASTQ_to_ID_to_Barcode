#!/usr/bin/env bash
echo "Starting Thomas PACBB analysis"
t=0
acc=9999
./main.py -midLen 25 --in-fastq "./FASTA/PacBio_Cleanreads_THOMAS/PACBB_${acc}_clean.fastq" -m "./FASTA/PACBB/PacBio_Primer_Associations_20161101.xlsx" \
-af "GCAGTCGAACATGTAGCTGACTCAGGTCAC" -ar "TGGATCACTTGTGCAAGCATCACATCGTAG" -pf "RKTCAACMAATCATAAAGATATTGG" \
-pr "TAAACTTCWGGRTGWCCAAAAAATCA" -ps "" -rs "./FASTA/PACBB/9173_Sanger_ref_seqs.fasta" -t $t -o "./RESULTS_OUT/ThomasResults/PACBB_thomas_t${t}_${acc}.txt"



