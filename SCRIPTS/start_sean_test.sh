#!/usr/bin/env bash

echo "Starting Sean job-151-CCDB-SQL-010_NGSFT24pM_CCS2_999-ccs analysis"
t=2
acc=999 #-midLen 25
~/PycharmProjects/PacBio_QC_seqs/main.py  --in-fastq "/Users/kirill/PycharmProjects/PacBio_QC_seqs/FASTA/Assembly_Sean/job-151-CCDB-SQL-010_NGSFT24pM_CCS2_999-ccs.fastq" \
-mid-fwd "/Users/kirill/PycharmProjects/PacBio_QC_seqs/PACBIO/9999_fwd.txt" \
-mid-rev "/Users/kirill/PycharmProjects/PacBio_QC_seqs/PACBIO/9999_rev.txt" \
-m "/Users/kirill/PycharmProjects/PacBio_QC_seqs/FASTA/Assembly_Sean/NGS-00037_MID_Map.xlsx" \
-af "GCAGTCGAACATGTAGCTGACTCAGGTCAC" -ar "TGGATCACTTGTGCAAGCATCACATCGTAG" -pf "RKTCAACMAATCATAAAGATATTGG" \
-pr "TAAACTTCWGGRTGWCCAAAAAATCA" -ps "" \
-cleanReads No \
-writeReads2Fastas Yes \
-t $t -o "/Users/kirill/PycharmProjects/PacBio_QC_seqs/TEST/SeanSeq_t${t}_${acc}.txt"