#!/usr/bin/env bash
source activate python_dev
echo "Starting Thomas PACBC analysis"
t=2
acc=99
./main.py -midLen 25 --in-fastq "./FASTA/PacBio_Cleanreads_THOMAS/PACBC_${acc}_notrim.fastq" \
-mid-fwd "/Users/kirill/PycharmProjects/PacBio_QC_seqs/PACBIO/9999_fwd.txt" \
-mid-rev "/Users/kirill/PycharmProjects/PacBio_QC_seqs/PACBIO/9999_rev.txt" \
-m "./PACBIO/PACBC_9120records_MIDtoSampleAssociation_SwapCorrected_EVZ20161221.xlsx" -af "GCAGTCGAACATGTAGCTGACTCAGGTCAC" \
-ar "TGGATCACTTGTGCAAGCATCACATCGTAG" -pf "RKTCAACMAATCATAAAGATATTGG" -pr "TAAACTTCWGGRTGWCCAAAAAATCA" -ps "" -rs "./FASTA/PACBC/9120_PacBio_ref_seqs.fasta" \
-t $t -o "./RESULTS_OUT/ThomasResults/PACBC/99_results/PACBC_thomas_t${t}_${acc}.txt"
