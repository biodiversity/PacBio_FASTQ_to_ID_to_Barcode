# QC of sequences from PacBio sequencer
There is a need to create and automatic pipeline that will "clean" 
the PacBio raw reads (in FASTA format), identify barcode and assign it to samples (i.e. plate wells). 
The first test dataset from PacBio sequencer is being provided by Thomas B. 

## Install
Requires the following libraries not present in standard python 2.7 install:
 - `tre` approximate regex matching [lib link](https://pypi.python.org/pypi/tre/0.8.0)
 - `Bio` biopythonlibrary [lib link](http://biopython.org/wiki/Getting_Started)


## Run 
Get help information on the key parameters run:
```
 ./CCS2ReadsDemultiplexer.py -h
```
Example run script for demux job:
```
./CCS2ReadsDemultiplexer.py --in-fastq "./FASTA/99.9.fastq" \
-af "GCAGTCGAACATGTAGCTGACTCAGGTCAC" -ar "TGGATCACTTGTGCAAGCATCACATCGTAG" \
-pf "RKTCAACMAATCATAAAGATATTGG" -pr "TAAACTTCWGGRTGWCCAAAAAATCA" -ps "GGTAG" \
-rs "./FASTA/9120_PacBio_ref_seqs.fasta" -t 0 -cleanReads No -writeReads2Fastas Yes -o "./RESULTS_OUT/tmp.txt"
```

## Output
Text tab-delimited file containing demultiplexed output (i.e. reads mapped to samples).
These reads need to be later assembled into final consensus sequences (one per sample) via [MappedReads2FinalBarcode](https://gitlab.com/biodiversity/MappedReads2Barcode).

```
## Output       /Users/kirill/PycharmProjects/PacBio_QC_seqs/RESULTS_OUT/Alkaline_SPRI_lysis_May17/JobID_157_t0.txt
## FASTQ file   /Users/kirill/PycharmProjects/PacBio_QC_seqs/DATA/May17/job-157-CCDB-SQL-011_Alkaline_1_CCS2-999-ccs.fastq
## Meta file    /Users/kirill/PycharmProjects/PacBio_QC_seqs/DATA/May17/CCDB-SQL-11-plate-layouts-2017-05-15-Primer_mappingV2.xlsx
## Match threshold tolerance    0
## Match length in bp (0=all read len)  25
ProcessID       SampleID        PlateID Coordinate      DescriptiveID   Barcode Nreads  OvlpDist2Ref    Indels  Subst   Len(bp) MaxOverlap(bp)  %Error2Ref      RefSeqID
PACBD191-17     BIOUG32374-A01  PBPCR1-101      A02     PACBD191-17[0001_AsF_lbc13+0002_AsR_lbc260_rc]_1_1reads GGTAGACACTGACGTCGCGACGCAGTCGAACATGTAGCTGACTCAGGTCACGTTCAACCAATCATAAAGATATTGGAACGTTATATTTTATTTTTGGAATTTGAGCAGGAATAGTAGGAACTTCTCTTAGTTTATTAATTCGAGCAGAATTGGGAAACCCAGGTTCTTTAATTGGTGATGATCAAATCTACAATACTATTGTAACAGCTCATGCTTTTATTATAATTTTTTTTATAGTAATACCAATTATAATCGGAGGATTTGGTAATTGATTAGTTCCTTTAATATTAGGAGCCCCAGATATAGCTTTTCCTCGAATAAATAATATAAGTTTTTGACTTCTCCCACCTTCAATCATATTATTAATTTCCAGAAGAATTGTAGAAAATGGTGCAGGAACAGGATGAACAGTATATCCTCCTTTATCTGCAAATATTGCTCATAGAGGATCTTCTGTTGATTTAGCTATCTTTTCTTTACATCTTGCTGGTATCTCTTCTATTTTAGGAGCTATTAATTTTATTACTACTATTATTAATATACGACTTAACAATATAATATTTGATCAATTACCTCTATTTGTATGAGCTGTTGGAATTACAGCCTTATTACTTTTACTTTCCCTCCCAGTACTAGCTGGGGCGATTACTATATTATTAACAGATCGGAATCTTAATACCTCTTTTTTGACCCAGCTGGAGGGGGAGACCCTATTCTGTACCAACATTTATTTTGATTTTTGGTACACCAGAAGTTTACTACGATGTGATGCTTGCACAAGTGATCCAGTGCACTCGCGCTCTCCTACC       1       1       0       1       809     588     0.170068027211  PHAAG101-17|Hellinsia|BIOUG32374-A01
```

## Parameters

```
usage: CCS2ReadsDemultiplexer.py  [-h] [-i] [-m] [-mid-fwd] [-mid-rev] [-af] [-ar] [-pf] [-pr]
               [-rs] [-ps] [-t] [-cleanReads CLEANREADS]
               [-writeReads2Fastas READS2FASTAS] [-midLen] [-o]

Reads FASTQ raw CCS reads from PacBio sequencers and identifies per sample
barcodes

optional arguments:
  -h, --help            show this help message and exit
  -i , --in-fastq       Input path to FASTQ file with sequences to demultiplex
  -m , --meta-data      Input path to META file (see Meta-data format requirements)
  -mid-fwd , --mid-fwd-seqs
                        Input path to forward MID-tag sequences text files [*]
  -mid-rev , --mid-rev-seqs
                        Input path to reverse MID-tag sequences text files [*]
  -af , --adapter-fw    Input forward adapter sequence (e.g. GCAGTCGAACATGTAGCTGACTCAGGTCAC)
  -ar , --adapter-rev   Input reverse adapter sequence (e.g. TGGATCACTTGTGCAAGCATCACATCGTAG)
  -pf , --primer-fw     Input Primer forward sequence (e.g. RKTCAACMAATCATAAAGATATTGG)
  -pr , --primer-rev    Input Primer reverse sequence (e.g. TAAACTTCWGGRTGWCCAAAAAATCA)
  -rs , --ref-seq       Input file with reference barcode sequences (optional) [**]
  -ps , --pad-seq       Input PAD sequence (e.g. GGTAG)
  -t , --match-error    Max mismatch error tolerance (e.g., 0 = exact match) [***]
  -cleanReads           Clean the reads from technical seqs (adapters, pad sequences, MID tags, primers). Possible values: Yes, No
  -writeReads2Fastas    Write reads to corresponding fasta files name {SampleID}.fasta
  -midLen , --mid-tag-regLen
                        Region lenght where to search for mid-tags from both ends of the read [****]
                        
  -mid-match-logic      Match MIDs in CCS2 read at both ends in pair-wise way (AND) or only at either end (OR) (optional. Default is AND)
  -o , --output         Output file to store results (e.g. output.txt)
```

[*] - the MID-tag sequences text files are in 2 column format (e.g. 0001_AsF_lbc13	ACACTGACGTCGCGAC) 

[**] - reference sequences fasta file with the title line containing a sample id (e.g. BIOUG32372-A03)

[***] - define number of mismatches to the MID seqeunces you can tolerate (e.g. -t 0 represent an exact match) 

[****] - either use entire read (e.g. `-midLen 0`) or only number of base-pairs from start and end of the read to search fro MID tags (e.g. `-midLen 25`)

## Files
 * [`METADATA_example.xlsx`](./INFO/METADATA_example.xlsx) - meta data example file with mapping info on the MID sequences to sampleIDs  
 for each well of the 384 plate. Should contain the specific header names (see meta requirements)
 * [`Primers.docx`](./INFO/Primers.docx) contains info on PacBio CCS2 sequence blocks (i.e. PAD, MID, PB-1 adapter, COI primer sequences)
 * [`9999_fwd.txt`](./DATA/Primers/9999_fwd.txt) forward 16-mer MID tag sequences in a 2 column format (name, sequence)
 * [`9999_rev.txt`](/DATA/Primers/9999_rev.txt) reverse 16-mer MID tag sequences in a 2 column format (name, sequence)
 
 
## Meta-data format requirements
The input meta-data (e.g.`PACBC_9120records_MIDtoSampleAssociation_EVZ20161214.xlsx`) 
should contain the following headers: `SampleID`, `ProcessID`, `384_Plate ID`,
 `Coordinate`, `Forward`, `Reverse`. 
 
 For example,
 
 ```
   SampleID                 ProcessID  384_Plate ID Coordinate  Forward      Reverse
   BIOUG32372-A01.PacBio	PACBD001-17	PBPCR1-101	A01 	0001_AsF_lbc13	0001_AsR_lbc76_rc
 ```

  
## Aims
 1. identify DNA barcodes in the speciments
 1. remove duplicated sequences
 1. develop QC control steps (e.g., correct DNA barcode orientation if flipped)
 1. standalone program (i.e. minimum dependencies)
 
## Algorithm
 1. Take each read
 1. Identify directionality of a read via MID tags or COI primers
 1. Identify a pair of MID tags. 
 1. Map read to the sample (i.e. 384 PlateID)
 1. Repeat for each read
 1. Determine number of _unique_ reads per sample (i.e. well) (i.e. demultiplexing) 
 
## Notes
 1. Each PacBio primer is composed of the following sequences (see example)
   * PAD_F->MID_F->PB1_Adapter_F->COI-PrimerF
   * PAD_R->MID_R->PB1_Adapter_R->COI-PrimerR
 1. Example FASTA read
    * sequencing can start at any site and contain repeats 
    * some primer sites can be missing due to degradation starting from PAD sequence
    


## Examples
The PacBio reads are assembled from longer sub-read seqeunces using the CCS2 algorithm from 
pbsmrtpipe or SMRTLink server

Example PacBio read after CCS2
 
```
PAD    MID              adapter                         COI primer                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  COI primer(rev)          adapter                         MID(rev)   PAD
GGTAGGCGAGACTCAGCTCTGGCAGTCGAACATGTAGCTGACTCAGGTCACAGTCAACAAATCATAAAGATATTGGAACTCTATATTTTATATTCGGAGCATGAGCAGGAATAGTAGGTACTTCCCTAAGTATTCTAATTCGAGCTGAATTAGGCCACCCAGGTTCCCTAATTGGTGATGATCAAATTTATAATGTAATTGTTACAGCTCATGCTTTTGTCATAATTTTTTTTATAGTAATACCAATTATAATTGGTGGGTTTGGTAATTGATTAGTCCCATTAATATTAGGAGCTCCAGATATAGCTTTCCCCCGAATAAATAATATAAGATTTTGATTATTACCACCTTCATTAACTTTATTGATAATAAGCAGTATAATTGATAATGGAGCTGGTACTGGATGAACAGTTTACCCTCCCCTATCTTCTAATATTGCCCACGGAGGAGCATCAGTTGATTTAGCTATCTTTTCTCTTCATCTTGCCGGTATTTCTTCAATTCTAGGAGCTGTAAACTTCATTACAACAGTAATTAATATACGATCTGCAGGTATTACTTTTGACCGAATACCTTTATTTGTATGAGCTGTAGTTATTACAGCTTTATTATTACTTTTATCTTTACCTGTATTAGCAGGAGCTATTACTATACTACTTACTGATCGAAATTTCAATACATCTTTTTTTGACCCTGCAGGAGGAGGAGATCCAATTTTATACCAACATTTATTCTGATTTTTTGGTACATCCTGAAGTTTACTACGATGTGATGCTTGCACAAGTGATCCAAGCGTAGCGCGCGTCACTACC
```

  
 